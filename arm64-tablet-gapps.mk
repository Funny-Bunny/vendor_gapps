# Copyright (C) 2017 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#inherit common gapps
$(call inherit-product, vendor/gapps/common-gapps.mk)

#arm64 live wallpapers packages
PRODUCT_PACKAGES += \
    WallpapersBReel \
    libgdx.so \
    libgeswallpapers-jni.so \
    libjpeg.so

#facelock librarys
PRODUCT_COPY_FILES += \
    vendor/gapps/lib/libfilterpack_facedetect.so:system/lib/libfilterpack_facedetect.so \
    vendor/gapps/lib64/libfacenet.so:system/lib64/libfacenet.so \
    vendor/gapps/lib64/libfilterpack_facedetect.so:system/lib64/libfilterpack_facedetect.so

#livewallpaper librarys
PRODUCT_COPY_FILES += \
    vendor/gapps/lib64/libgdx.so:system/lib64/libgdx.so \
    vendor/gapps/lib64/libgeswallpapers-jni.so:system/lib64/libgeswallpapers-jni.so

#velvet librarys
PRODUCT_COPY_FILES += \
    vendor/gapps/priv-app/Velvet/arm64/Velvet/lib/arm64/libbrotli.so:system/priv-app/Velvet/lib/arm64/libbrotli.so \
    vendor/gapps/priv-app/Velvet/arm64/Velvet/lib/arm64/libcronet.so:system/priv-app/Velvet/lib/arm64/libcronet.so \
    vendor/gapps/priv-app/Velvet/arm64/Velvet/lib/arm64/libframesequence.so:system/priv-app/Velvet/lib/arm64/libframesequence.so \
    vendor/gapps/priv-app/Velvet/arm64/Velvet/lib/arm64/libgoogle_speech_jni.so:system/priv-app/Velvet/lib/arm64/libgoogle_speech_jni.so \
    vendor/gapps/priv-app/Velvet/arm64/Velvet/lib/arm64/libgoogle_speech_micro_jni.so:system/priv-app/Velvet/lib/arm64/libgoogle_speech_micro_jni.so \
    vendor/gapps/priv-app/Velvet/arm64/Velvet/lib/arm64/libnativecrashreporter.so:system/priv-app/Velvet/lib/arm64/libnativecrashreporter.so \
    vendor/gapps/priv-app/Velvet/arm64/Velvet/lib/arm64/liboffline_actions_jni.so:system/priv-app/Velvet/lib/arm64/liboffline_actions_jni.so \
    vendor/gapps/priv-app/Velvet/arm64/Velvet/lib/arm64/libthird_party_brotli_dec_jni.so:system/priv-app/Velvet/lib/arm64/libthird_party_brotli_dec_jni.so